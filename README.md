# Javanaise shared object cache library

  

## Installation

  The  project can be built using `ant`.

| Command | Description |  
| ----------- | ----------- |
| `ant` | Build project |
| `ant clean` | Clean project |
| `ant doc` | Generate documentation |
| `ant coordinator` | Run the example coordinator |  
| `ant client` | Run the example IRC client (require coordinator running) |
| `ant tests` | Run burst tests |

Tests parameters can be tweaked by modifying `NB_CLIENTS`, `NB_OBJECTS` and `NB_ACCESS`   in `src/tests/BurstTester.java`

## IRC example

The IRC example client can read, write and flush a shared message managed by a coordinator using the associated buttons. When it reads or writes it acquires a lock  on the message which needs to be released using the `unlock` button.

## The Javanaise library

The documentation for the Javanaise library can be generated using `ant doc` and is located in the `doc/` folder.