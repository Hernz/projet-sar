package jvn.coordinator;

import jvn.JvnException;
import jvn.server.JvnRemoteServer;

import java.util.ArrayList;

/**
 * A class for storing and managing read and
 * write permissions for JvnServers on a JvnObject
 *
 * @author Louka Soret
 */
public class JvnPermissions {
    private ArrayList<JvnRemoteServer> readLockServers = new ArrayList<>();
    private JvnRemoteServer writeLockServer = null;

    public ArrayList<JvnRemoteServer> getServers() {
        ArrayList<JvnRemoteServer> res = new ArrayList<>(readLockServers);
        if (writeLockServer != null) {
            res.add(writeLockServer);
        }
        return res;
    }

    /**
     * Add a server to the read lock list
     *
     * @param server a javanaise remote server
     */
    public void lockRead(JvnRemoteServer server) {
        if (!readLockServers.contains(server)) {
            readLockServers.add(server);
        }
    }

    /**
     * Remove a server from the read lock list
     *
     * @param server a javanaise remote server
     */
    public void unlockRead(JvnRemoteServer server) {
        if (readLockServers.contains(server)) {
            readLockServers.remove(server);
        }
    }

    public void unlockRead() {
        readLockServers.clear();
    }

    /**
     * Set the write lock server
     *
     * @param server a javanaise remote server
     */
    public void lockWrite(JvnRemoteServer server) throws JvnException {
        if (writeLockServer == null) {
            writeLockServer = server;
        } else {
            throw new JvnException("The write lock is already taken");
        }
    }

    /**
     * unset the write lock server
     */
    public void unlockWrite() {
        writeLockServer = null;
    }

    /**
     * unset the write lock server and set it to read lock
     */
    public void unlockWriteForRead() throws JvnException {
        if (writeLockServer != null) {
            this.lockRead(writeLockServer);
            this.unlockWrite();
        } else {
            throw new JvnException("The write lock is not taken");
        }
    }

    public boolean isLockedWrite(){
        return writeLockServer != null;
    }

    public boolean isLockedRead(){
        return !readLockServers.isEmpty();
    }

    /**
     * Get the write lock server
     *
     * @return the JvnRemoteServer which has the write lock
     */
    public JvnRemoteServer getWriteLockServer() {
        return writeLockServer;
    }

    /**
     * Get the read lock server list
     *
     * @return the list of JvnRemoteServer which have the read lock
     */
    public ArrayList<JvnRemoteServer> getReadLockServers() {
        return readLockServers;
    }

    @Override
    public String toString() {
        return "Permissions : \n" +
                "- Read : " + readLockServers +
                "\n- Write : " + writeLockServer;
    }
}
