/***
 * JAVANAISE Implementation
 * JvnCoordImpl class
 * This class implements the Javanaise central coordinator
 * Contact:  
 *
 * Authors: 
 */

package jvn.coordinator;

import jvn.JvnException;
import jvn.object.JvnObject;
import jvn.object.JvnObjectImpl;
import jvn.server.JvnRemoteServer;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.io.Serializable;
import java.util.*;


public class JvnCoordImpl extends UnicastRemoteObject implements JvnRemoteCoord {


    // Map JON to JOI of JvnObjects
    private HashMap<Integer, String> joiToJon = new HashMap<>();

    // Map JOI to JvnPermissions
    private HashMap<Integer, JvnPermissions> joiToPermissions = new HashMap<>();

    private HashMap<Integer,JvnObject> orphans = new HashMap<>();

    private int idCounter = 0;

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    // A JVN coordinator is managed as a singleton
    private static JvnCoordImpl coordinator = null;

    /**
     * Default constructor
     *
     * @throws JvnException
     **/
    private JvnCoordImpl(int port) throws Exception {
        super(port);
    }

    public static JvnCoordImpl jvnGetCoord(int port) {
        if (coordinator == null) {
            try {
                coordinator = new JvnCoordImpl(port);
            } catch (Exception e) {
                return null;
            }
        }
        return coordinator;
    }

    /**
     * Allocate a NEW JVN object id (usually allocated to a
     * newly created JVN object)
     *
     * @throws java.rmi.RemoteException,JvnException
     **/
    public synchronized int jvnGetObjectId()
            throws java.rmi.RemoteException, JvnException {
        return ++idCounter;
    }

    /**
     * Associate a symbolic name with a JVN object
     *
     * @param jon : the JVN object name
     * @param jo  : the JVN object
     * @param js  : the remote reference of the JVNServer
     * @throws java.rmi.RemoteException,JvnException
     **/
    public synchronized void jvnRegisterObject(String jon, JvnObject jo, JvnRemoteServer js)
            throws java.rmi.RemoteException, JvnException {
        int joi = jo.jvnGetObjectId();
        JvnPermissions jvnPermissions = new JvnPermissions();
        jvnPermissions.lockWrite(js);
        joiToJon.put(joi, jon);
        joiToPermissions.put(joi, jvnPermissions);
    }

    /**
     * Get the reference of a JVN object managed by a given JVN server
     *
     * @param jon : the JVN object name
     * @param js  : the remote reference of the JVNServer
     * @throws java.rmi.RemoteException,JvnException
     **/
    public JvnObject jvnGetObject(String jon, JvnRemoteServer js)
            throws java.rmi.RemoteException, JvnException {
        return js.jvnGetObject(jon);
    }


    public static <T, E> T getKeyByValue(Map<T, E> map, E value) {
        for (Map.Entry<T, E> entry : map.entrySet()) {
            if (Objects.equals(value, entry.getValue())) {
                return entry.getKey();
            }
        }
        return null;
    }

    public synchronized JvnObject jvnLookupObject(String jon)
            throws java.rmi.RemoteException, JvnException {
        if (joiToJon.containsValue(jon)) {
            Integer joi = getKeyByValue(joiToJon, jon);
            JvnPermissions jvnPermissions = joiToPermissions.get(joi);
            if (jvnPermissions.isLockedWrite()) {
                return jvnGetObject(jon, jvnPermissions.getWriteLockServer());
            }
            if (jvnPermissions.isLockedRead()) {
                return jvnGetObject(jon, jvnPermissions.getReadLockServers().get(0));
            }
            if (orphans.containsKey(joi)){
                return orphans.get(joi);
            }
        }
        return null;
    }

    public synchronized JvnObject jvnLookupOrCreate(JvnRemoteServer js, String jon, Serializable object)
            throws JvnException, RemoteException {
        JvnObject jvnObject = null;
        if (joiToJon.containsValue(jon)) {
            Integer joi = getKeyByValue(joiToJon, jon);
            JvnPermissions jvnPermissions = joiToPermissions.get(joi);
            if (jvnPermissions.isLockedWrite()) {
                jvnObject = jvnGetObject(jon, jvnPermissions.getWriteLockServer());
            }
            if (jvnPermissions.isLockedRead()) {
                jvnObject = jvnGetObject(jon, jvnPermissions.getReadLockServers().get(0));
            }
            if (orphans.containsKey(joi)){
                jvnObject = orphans.get(joi);
            }
        }
        if (jvnObject == null){
            jvnObject = new JvnObjectImpl(++idCounter, object);
            jvnObject.setWritePermission(JvnObjectImpl.PERMISSIONS.CACHED);
            jvnObject.jvnUnLock();

            js.jvnRegisterObjectLocal(jon, jvnObject);

            int joi = jvnObject.jvnGetObjectId();
            JvnPermissions jvnPermissions = new JvnPermissions();
            jvnPermissions.lockWrite(js);
            joiToJon.put(joi, jon);
            joiToPermissions.put(joi, jvnPermissions);
        } else {
            jvnObject = new JvnObjectImpl(jvnObject.jvnGetObjectId(),jvnObject.jvnGetSharedObject());
            js.jvnRegisterObjectLocal(jon, jvnObject);
        }

        return jvnObject;
    }


    /**
     * Get a Read lock on a JVN object managed by a given JVN server
     *
     * @param joi : the JVN object identification
     * @param js  : the remote reference of the server
     * @return the current JVN object state
     * @throws java.rmi.RemoteException, JvnException
     **/
    public synchronized Serializable jvnLockRead(int joi, JvnRemoteServer js)
            throws java.rmi.RemoteException, JvnException {
        JvnPermissions objectPermissions = joiToPermissions.get(joi);
        Serializable updatedObject = null;
        int i;
        // Invalidate the writer for reader
        if (objectPermissions.isLockedWrite()){
            JvnRemoteServer server = objectPermissions.getWriteLockServer();
            // Get the latest version of the object
            updatedObject = server.jvnInvalidateWriterForReader(joi);
            objectPermissions.unlockWriteForRead();
        } else if (objectPermissions.isLockedRead()){
            // If there is no write lock then fetch the latest version of the object from the readers
            i = 0;
            do {
                updatedObject = jvnGetObject(joiToJon.get(joi),objectPermissions.getReadLockServers().get(i)).jvnGetSharedObject();
                i++;
            } while (updatedObject == null);
        } else if (orphans.containsKey(joi)){
            updatedObject = orphans.remove(joi).jvnGetSharedObject();
        }
        objectPermissions.lockRead(js); // Lock read
        return updatedObject;
    }

    /**
     * Get a Write lock on a JVN object managed by a given JVN server
     *
     * @param joi : the JVN object identification
     * @param js  : the remote reference of the server
     * @return the current JVN object state
     * @throws java.rmi.RemoteException, JvnException
     **/
    public synchronized Serializable jvnLockWrite(int joi, JvnRemoteServer js) throws java.rmi.RemoteException, JvnException {
        JvnPermissions objectPermissions = joiToPermissions.get(joi);
        Serializable updatedObject = null;
        if (objectPermissions.isLockedWrite()){
            // Invalidate the writer and fetch the object
            JvnRemoteServer server = objectPermissions.getWriteLockServer();
            updatedObject = server.jvnInvalidateWriter(joi);
            objectPermissions.unlockWrite();
        }
        if (objectPermissions.isLockedRead()){
            // Invalidate all the readers and fetch the object
            ArrayList<JvnRemoteServer> servers = new ArrayList<>(objectPermissions.getReadLockServers());
            JvnRemoteServer server;

            for(int i = 0;i<servers.size();i++){
                server = servers.get(i);
                server.jvnInvalidateReader(joi);
                if (updatedObject == null){
                    updatedObject = server.jvnGetObject(joiToJon.get(joi)).jvnGetSharedObject();
                }
            }
            objectPermissions.unlockRead();
        }
        if (orphans.containsKey(joi)){
            updatedObject = orphans.remove(joi).jvnGetSharedObject();
        }
        objectPermissions.lockWrite(js); // Lock write
        return updatedObject;
    }

    public synchronized void jvnFlush(int joi, JvnRemoteServer js)
            throws java.rmi.RemoteException, JvnException {
        JvnPermissions permission = joiToPermissions.get(joi);
        permission = joiToPermissions.get(joi);
        if (permission != null) {
            if (permission.getReadLockServers().contains(js)) {
                permission.unlockRead(js);
            }
            if (js.equals(permission.getWriteLockServer())) {
                permission.unlockWrite();
            }
        }
        orphans.put(joi,js.jvnGetObject(joiToJon.get(joi)));
    }

    /**
     * A JVN server terminates
     *
     * @param js : the remote reference of the server
     * @throws java.rmi.RemoteException, JvnException
     **/
    public synchronized void jvnTerminate(JvnRemoteServer js)
            throws java.rmi.RemoteException, JvnException {
        // Unlock all locks for the RemoteServer
        JvnPermissions permission;
        for ( Integer joi: joiToPermissions.keySet()) {
            jvnFlush(joi, js);
        }
    }

    public ArrayList<JvnRemoteServer> getKnownServers() {
        ArrayList<JvnRemoteServer> res = new ArrayList<>();
        for (JvnPermissions permission : joiToPermissions.values()) {
            res.addAll(permission.getServers());
        }
        return res;
    }

    public HashMap<Integer, String> getJoiToJon() {
        return joiToJon;
    }

    public HashMap<Integer, JvnPermissions> getJoiToPermissions() {
        return joiToPermissions;
    }

    public HashMap<Integer, JvnObject> getOrphans() {
        return orphans;
    }

}

 
