package jvn.object;

import jvn.JvnException;
import jvn.server.JvnServerImpl;

import java.io.Serializable;

/**
 * @author Louka Soret
 */
public class JvnObjectImpl implements JvnObject {

    public enum PERMISSIONS {
        NON_LOCKED,
        CACHED,
        LOCKED
    }

    private Integer id;
    private Serializable object;

    private PERMISSIONS read_right = PERMISSIONS.NON_LOCKED;

    private PERMISSIONS write_right = PERMISSIONS.NON_LOCKED;

    public JvnObjectImpl(Integer id, Serializable object) {
        this.id = id;
        this.object = object;
    }

    /**
     * Get the object identification
     *
     * @throws JvnException
     **/
    @Override
    public int jvnGetObjectId() throws JvnException {
        return id;
    }

    /**
     * Get the shared object associated to this JvnObject
     *
     * @throws JvnException
     **/
    @Override
    public Serializable jvnGetSharedObject() throws JvnException {
        return object;
    }

    @Override
    public void jvnSetSharedObject(Serializable object) {
        this.object = object;
    }

    /**
     * Get a Read lock on the shared object
     *
     * @throws JvnException
     **/
    @Override
    public void jvnLockRead() throws JvnException {
        Serializable updatedObject;
        if (read_right == PERMISSIONS.NON_LOCKED) {
            updatedObject = JvnServerImpl.jvnGetServer().jvnLockRead(id);
            if (updatedObject != null){
                object = updatedObject;
            }
        }
        read_right = PERMISSIONS.LOCKED;
        //System.out.println("Object "+id+" got read lock ("+read_right+","+write_right+")");
    }

    /**
     * Get a Write lock on the object
     *
     * @throws JvnException
     **/
    @Override
    public void jvnLockWrite() throws JvnException {
        Serializable updatedObject;
        if (write_right == PERMISSIONS.NON_LOCKED) {
            updatedObject = JvnServerImpl.jvnGetServer().jvnLockWrite(id);
            if (updatedObject != null){
                object = updatedObject;
            }
        }
        write_right = PERMISSIONS.LOCKED;
        //System.out.println("Object "+id+" got write lock ("+read_right+","+write_right+")");
    }

    /**
     * Unlock  the object
     *
     * @throws JvnException
     **/
    @Override
    public void jvnUnLock() throws JvnException {
        read_right = (read_right == PERMISSIONS.NON_LOCKED) ? PERMISSIONS.NON_LOCKED : PERMISSIONS.CACHED;
        write_right = (write_right == PERMISSIONS.NON_LOCKED) ? PERMISSIONS.NON_LOCKED : PERMISSIONS.CACHED;
        //System.out.println("Object "+id+" unlocked ("+read_right+","+write_right+")");
        synchronized (this){
            notify();
        }
    }

    /**
     * Invalidate the Read lock of the JVN object
     *
     * @throws JvnException
     **/
    @Override
    synchronized public void jvnInvalidateReader() throws JvnException {
        while (read_right == PERMISSIONS.LOCKED) {
            try {
                    wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        read_right = PERMISSIONS.NON_LOCKED;
        //System.out.println("Object "+id+" got read lock invalidated ("+read_right+","+write_right+")");
    }

    /**
     * Invalidate the Write lock of the JVN object
     *
     * @return the current JVN object state
     * @throws JvnException
     **/
    @Override
    synchronized public Serializable jvnInvalidateWriter() throws JvnException {
        while (write_right == PERMISSIONS.LOCKED) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        write_right = PERMISSIONS.NON_LOCKED;
        //System.out.println("Object "+id+" got write lock invalidated (non locked) ("+read_right+","+write_right+")");
        return jvnGetSharedObject();
    }

    /**
     * Reduce the Write lock of the JVN object
     *
     * @return the current JVN object state
     * @throws JvnException
     **/
    @Override
    synchronized public Serializable jvnInvalidateWriterForReader() throws JvnException {
        while (write_right == PERMISSIONS.LOCKED) {
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        write_right = PERMISSIONS.NON_LOCKED;
        read_right = PERMISSIONS.CACHED;

        //System.out.println("Object "+id+" got write lock invalidated for reader ("+read_right+","+write_right+")");
        return jvnGetSharedObject();
    }

    public void setWritePermission(PERMISSIONS write_right) {
        this.write_right = write_right;
    }

    /**
     * @return true if is locked either in write or read
     * false else
     */
    public boolean isLocked(){
        return read_right == PERMISSIONS.LOCKED
                || write_right == PERMISSIONS.LOCKED;
    }

    /**
     * Flush the managed object
     */
    public void flush() throws JvnException {
        jvnInvalidateReader();
        jvnInvalidateWriter();
        object = null;
    }

    /**
     * compute the likelihood of the object of being flushed
     * @return the flush score
     */
    public int getFlushScore(){
        int score = 0;

        if(read_right == PERMISSIONS.LOCKED || write_right == PERMISSIONS.LOCKED){
            return score;
        }
        if (read_right == PERMISSIONS.CACHED){
            score += 2;
        } else if (read_right == PERMISSIONS.NON_LOCKED){
            score += 3;
        }
        if (write_right == PERMISSIONS.CACHED){
            score += 1;
        } else if (write_right == PERMISSIONS.NON_LOCKED){
            score += 3;
        }

        return score;
    }
}
