package jvn.object;

import jvn.AnnotationFinder;
import jvn.JvnException;
import jvn.annotations.*;
import jvn.server.JvnServerImpl;

import java.io.Serializable;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class JvnObjectProxy implements InvocationHandler {

    private JvnObject jvnObject;

    /**
     * Create an InvocationHandler for the object and
     * encapsulate it in a JvnObject
     * @param object any serializable object
     * @param name object name
     */
    private JvnObjectProxy(Serializable object, String name){
        try {
            JvnServerImpl js = JvnServerImpl.jvnGetServer();

            this.jvnObject = js.jvnLookupOrCreate(name, object);

            /*
            // Try to lookup the object name on the server
            this.jvnObject = js.jvnLookupObject(name);
            // If it doesn't exist, create it
            if (this.jvnObject == null){
                this.jvnObject = js.jvnCreateObject(object);
                this.jvnObject.jvnUnLock();
                js.jvnRegisterObject(name, this.jvnObject);
            }
             */
        } catch (JvnException e) {
            e.printStackTrace();
        }
    }

    /**
     * Create a new proxy for the object
     * @param object any serializable object
     * @param name object name
     * @return a new proxy for the object
     */
    public static Object newInstance(Serializable object, String name){
        if (AnnotationFinder.isAnnotationPresentNested(object.getClass(),JvnManaged.class)){
            return java.lang.reflect.Proxy.newProxyInstance(
                    object.getClass().getClassLoader(),
                    object.getClass().getInterfaces(),
                    new JvnObjectProxy(object,name));
        } else {
            return object;
        }
    }


    @Override
    public Object invoke(Object object, Method method, Object[] arguments) throws Throwable {
        Object result = null;

        // Debug related booleans
        boolean debugAnnotationPresent = AnnotationFinder.isAnnotationPresentNested(object.getClass(), JvnDebug.class);
        boolean manualUnlock = false;
        if(debugAnnotationPresent){
            manualUnlock = AnnotationFinder.getAnnotationNested(object.getClass(),JvnDebug.class).manualUnlock();
        }

        if (AnnotationFinder.isAnnotationPresentNested(object.getClass(),JvnManaged.class)){
            // The called method belongs to a jvn managed object
            Serializable o;
            if (AnnotationFinder.isAnnotationPresentNested(method,JvnRead.class)){
                // The method is of type read
                this.jvnObject.jvnLockRead();
                o = this.jvnObject.jvnGetSharedObject();
                result = method.invoke(o,arguments);
                if(!debugAnnotationPresent || ! manualUnlock){
                    this.jvnObject.jvnUnLock();
                }
            } else if (AnnotationFinder.isAnnotationPresentNested(method,JvnWrite.class)) {
                // The method is of type write
                this.jvnObject.jvnLockWrite();
                o = this.jvnObject.jvnGetSharedObject();
                result = method.invoke(o, arguments);
                if (!debugAnnotationPresent || !manualUnlock) {
                    this.jvnObject.jvnUnLock();
                }
            } else if (AnnotationFinder.isAnnotationPresentNested(method, JvnUnlock.class)) {
                // The method is of type unlock (debug)
                o = this.jvnObject.jvnGetSharedObject();
                result = method.invoke(o, arguments);
                this.jvnObject.jvnUnLock();
            }
        } else {
            // The called method does not belongs to a jvn managed object
            result = method.invoke(object, arguments);
        }

        return result;
    }
}
