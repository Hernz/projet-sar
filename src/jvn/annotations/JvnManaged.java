package jvn.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Mark the class as managed by Jvn
 */
// The annotation is available at execution time
@Retention(RetentionPolicy.RUNTIME)
// The annotation is associated with a type (Class, interface)
@Target(ElementType.TYPE)
public @interface JvnManaged {}
