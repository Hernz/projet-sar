/***
 * JAVANAISE API
 * JvnLocalServer interface
 * Defines the local interface provided by a JVN server 
 * An application uses the Javanaise service through the local interface provided by the Jvn server 
 * Contact: 
 *
 * Authors: 
 */

package jvn.server;

import jvn.JvnException;
import jvn.object.JvnObject;

import java.io.Serializable;

/**
 * Local interface of a JVN server  (used by the applications).
 * An application can get the reference of a JVN server through the static
 * method jvnGetServer() (see  JvnServerImpl).
 */

public interface JvnLocalServer {

    /**
     * create of a JVN object
     *
     * @param jos : the JVN object state
     * @return the JVN object
     * @throws JvnException
     **/
    JvnObject jvnCreateObject(Serializable jos)
            throws JvnException;

    /**
     * Associate a symbolic name with a JVN object
     *
     * @param jon : the JVN object name
     * @param jo  : the JVN object
     * @throws JvnException
     **/
    void jvnRegisterObject(String jon, JvnObject jo)
            throws JvnException;

    /**
     * Get a Read lock on a JVN object
     *
     * @param joi : the JVN object identification
     * @return the current JVN object state
     * @throws JvnException
     **/
    Serializable jvnLockRead(int joi)
            throws JvnException;

    /**
     * Get a Write lock on a JVN object
     *
     * @param joi : the JVN object identification
     * @return the current JVN object state
     * @throws JvnException
     **/
    Serializable jvnLockWrite(int joi)
            throws JvnException;


    /**
     * The JVN service is not used anymore by the application
     *
     * @throws JvnException
     **/
    void jvnTerminate()
            throws JvnException;

    /**
     * Try to connect to the coordinator via RMI
     * @param host coordinator adress
     * @param port coordinator port
     */
    void jvnConnectToCoord(String host, int port);

    /**
     * Try to find the object of name jon
     * in the LocalServer (here) or asks
     * the coordinator to find it on him or
     * other servers.
     *
     * @param jon the name of the object
     * @return the jvnObject found or null
     * @throws JvnException
     */
    JvnObject jvnLookupObject(String jon) throws JvnException;

    /**
     * Try to find the object of name jon
     * in the LocalServer (here) or asks
     * the coordinator to find OR create it.
     *
     * @param jon the name of the object
     * @param object the object to be shared
     * @return the jvnObject found on the coordinator
     * or created with object
     * @throws JvnException
     */
    JvnObject jvnLookupOrCreate(String jon, Serializable object) throws JvnException;

    /**
     * flush an object to the coordinator if possible or raise
     * an exception
     *
     * @throws JvnException
     */
    void jvnFlush() throws JvnException;
}
