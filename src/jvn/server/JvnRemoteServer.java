/***
 * JAVANAISE API
 * JvnRemoteServer interface
 * Defines the remote interface provided by a JVN server 
 * This interface is intended to be invoked by the Javanaise coordinator 
 * Contact: 
 *
 * Authors: 
 */

package jvn.server;

import jvn.JvnException;
import jvn.object.JvnObject;

import java.rmi.*;
import java.io.*;


/**
 * Remote interface of a JVN server (used by a remote JvnCoord)
 */

public interface JvnRemoteServer extends Remote {

    /**
     * Invalidate the Read lock of a JVN object
     *
     * @param joi : the JVN object id
     * @throws java.rmi.RemoteException,JvnException
     **/
    void jvnInvalidateReader(int joi)
            throws java.rmi.RemoteException, JvnException;

    /**
     * Invalidate the Write lock of a JVN object
     *
     * @param joi : the JVN object id
     * @return the current JVN object state
     * @throws java.rmi.RemoteException,JvnException
     **/
    Serializable jvnInvalidateWriter(int joi)
            throws java.rmi.RemoteException, JvnException;

    /**
     * Reduce the Write lock of a JVN object
     *
     * @param joi : the JVN object id
     * @return the current JVN object state
     * @throws java.rmi.RemoteException,JvnException
     **/
    Serializable jvnInvalidateWriterForReader(int joi)
            throws java.rmi.RemoteException, JvnException;

    /**
     * Provide the reference of a JVN object being given its symbolic name
     *
     * @param jon : the JVN object name
     * @return the JVN object
     * @throws JvnException
     **/
    JvnObject jvnGetObject(String jon)
            throws java.rmi.RemoteException, JvnException;

    /**
     * Associate a symbolic name with a JVN object
     *
     * @param jon : the JVN object name
     * @param jo  : the JVN object
     * @throws java.rmi.RemoteException
     * @throws JvnException
     */
    void jvnRegisterObjectLocal(String jon, JvnObject jo)
            throws java.rmi.RemoteException, JvnException;
}

 
