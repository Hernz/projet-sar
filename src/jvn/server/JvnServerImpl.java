/***
 * JAVANAISE Implementation
 * JvnServerImpl class
 * Implementation of a Jvn server
 * Contact: 
 *
 * Authors: 
 */

package jvn.server;

import jvn.JvnException;
import jvn.coordinator.JvnRemoteCoord;
import jvn.object.JvnObject;
import jvn.object.JvnObjectImpl;

import java.lang.reflect.Array;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;


public class JvnServerImpl extends UnicastRemoteObject implements JvnLocalServer, JvnRemoteServer {

    // Map JON to JOI of JvnObjects
    private HashMap<String, Integer> jonToJoi = new HashMap<String, Integer>();
    // Map JOI to jvnObject
    private HashMap<Integer, JvnObject> joiToJvnObject = new HashMap<Integer, JvnObject>();

    private JvnRemoteCoord coordinator;

    private static final long serialVersionUID = 1L;
    // A JVN server is managed as a singleton
    private static JvnServerImpl js = null;

    /**
     * Default constructor
     *
     * @throws JvnException
     **/
    private JvnServerImpl() throws Exception {
        super();
    }

    /**
     * Static method allowing an application to get a reference to
     * a JVN server instance
     *
     * @throws JvnException
     **/
    public static JvnServerImpl jvnGetServer() {
        if (js == null) {
            try {
                js = new JvnServerImpl();
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
        return js;
    }

    /**
     * Try to connect to the coordinator via RMI
     * @param host coordinator adress
     * @param port coordinator port
     */
    public void jvnConnectToCoord(String host, int port) {
        try {
            Registry registry = LocateRegistry.getRegistry(host, port);
            js.coordinator = (JvnRemoteCoord) registry.lookup("jvnCoordinator");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * The JVN service is not used anymore
     *
     * @throws JvnException
     **/
    public void jvnTerminate()
            throws JvnException {
        try {
            coordinator.jvnTerminate(js);
            joiToJvnObject.clear();
            jonToJoi.clear();
            coordinator = null;
            js = null;
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    /**
     * creation of a JVN object
     *
     * @param object : the JVN object state
     * @throws JvnException
     **/
    public JvnObject jvnCreateObject(Serializable object) throws JvnException {
        Integer id = null;
        try {
            id = coordinator.jvnGetObjectId();
        } catch (RemoteException e) {
            e.printStackTrace();
            return null;
        }
        JvnObject newObject = new JvnObjectImpl(id, object);
        newObject.setWritePermission(JvnObjectImpl.PERMISSIONS.CACHED);
        return newObject;
    }

    /**
     * Associate a symbolic name with a JVN object
     * And register on the coordinator
     *
     * @param jon : the JVN object name
     * @param jo  : the JVN object
     * @throws JvnException
     **/
    public void jvnRegisterObject(String jon, JvnObject jo)
            throws JvnException {
        int joi = jo.jvnGetObjectId();
        jonToJoi.put(jon, joi);
        joiToJvnObject.put(joi, jo);
        try {
            coordinator.jvnRegisterObject(jon, jo, js);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    /**
     * Associate a symbolic name with a JVN object
     *
     * @param jon : the JVN object name
     * @param jo  : the JVN object
     * @throws java.rmi.RemoteException
     * @throws JvnException
     */
    public void jvnRegisterObjectLocal(String jon, JvnObject jo)
            throws java.rmi.RemoteException, JvnException {
        int joi = jo.jvnGetObjectId();
        jonToJoi.put(jon, joi);
        joiToJvnObject.put(joi, jo);
    }

    /**
     * Provide the reference of a JVN object being given its symbolic name
     *
     * @param jon : the JVN object name
     * @return the JVN object
     * @throws JvnException
     **/
    public JvnObject jvnGetObject(String jon)
            throws JvnException {
        if (jonToJoi.containsKey(jon) && joiToJvnObject.containsKey(jonToJoi.get(jon))) {
            return joiToJvnObject.get(jonToJoi.get(jon));
        } else {
            return null;
        }
    }

    /**
     * Try to find the object of name jon
     * in the LocalServer (here) or asks
     * the coordinator to find OR create it.
     *
     * @param jon the name of the object
     * @param object the object to be shared
     * @return the jvnObject found on the coordinator
     * or created with object
     * @throws JvnException
     */
    public JvnObject jvnLookupOrCreate(String jon, Serializable object) throws JvnException {
        try {
            JvnObject jvnObject = coordinator.jvnLookupOrCreate(jvnGetServer(), jon, object);
            if (jonToJoi.containsKey(jon) && joiToJvnObject.containsKey(jonToJoi.get(jon))) {
                JvnObject localObject = joiToJvnObject.get(jonToJoi.get(jon));
                localObject.jvnSetSharedObject(jvnObject.jvnGetSharedObject());
                return localObject;
            } else {
                return jvnObject;
            }
        } catch (RemoteException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Try to find the object of name jon
     * in the LocalServer (here) or asks
     * the coordinator to find it on him or
     * other servers.
     *
     * @param jon the name of the object
     * @return the jvnObject found or null
     * @throws JvnException
     */
    public JvnObject jvnLookupObject(String jon)
            throws JvnException {
        try {
            JvnObject jvnObject = coordinator.jvnLookupObject(jon);
            if (jvnObject != null) {
                if (jonToJoi.containsKey(jon) && joiToJvnObject.containsKey(jonToJoi.get(jon))) {
                    JvnObject localObject = joiToJvnObject.get(jonToJoi.get(jon));
                    localObject.jvnSetSharedObject(jvnObject.jvnGetSharedObject());
                    return localObject;
                } else {
                    JvnObject jo = new JvnObjectImpl(jvnObject.jvnGetObjectId(),jvnObject.jvnGetSharedObject());
                    int joi = jo.jvnGetObjectId();
                    jonToJoi.put(jon, joi);
                    joiToJvnObject.put(joi, jo);
                    return jo;
                }
            }
            return null;
        } catch (RemoteException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * flush an object to the coordinator if possible or raise
     * an exception
     *
     * @throws JvnException
     */
    public void jvnFlush() throws JvnException {
        JvnObject jvnObject;
        ArrayList<Integer> joiArray = new ArrayList<>(joiToJvnObject.keySet());

        // Sort cached object by likelihood of being flushed
        joiArray.sort((joi1, joi2) -> {
            JvnObject jvnObject1 = joiToJvnObject.get(joi1);
            JvnObject jvnObject2 = joiToJvnObject.get(joi2);
            return jvnObject1.getFlushScore() - jvnObject2.getFlushScore();
        });

        int i = 0;
        jvnObject = joiToJvnObject.get(joiArray.get(i));

        // Find a flush candidate (can't flush a locked object)
        while(i < joiArray.size() && jvnObject.isLocked()){
            i++;
        }

        if (!jvnObject.isLocked()){ // Flush candidate found
            try {
                coordinator.jvnFlush(joiArray.get(i),this);
                jvnObject.flush();
                System.out.println("Local Server flushed jvnObject "+jvnObject.jvnGetObjectId());
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        } else if(i >= joiArray.size()){ // All objects in use
            throw new JvnException("Could not flush : all cached objects are in use");
        }
    }

    /**
     * Get a Read lock on a JVN object
     *
     * @param joi : the JVN object identification
     * @return the current JVN object state
     * @throws JvnException
     **/
    public Serializable jvnLockRead(int joi) throws JvnException {
        Serializable object = null;
        try {
            object = coordinator.jvnLockRead(joi, this);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return object;
    }

    /**
     * Get a Write lock on a JVN object
     *
     * @param joi : the JVN object identification
     * @return the current JVN object state
     * @throws JvnException
     **/
    public Serializable jvnLockWrite(int joi) throws JvnException {
        Serializable object = null;
        try {
            object = coordinator.jvnLockWrite(joi, this);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return object;
    }


    /**
     * Invalidate the Read lock of the JVN object identified by id
     * called by the JvnCoord
     *
     * @param joi : the JVN object id
     * @return void
     * @throws java.rmi.RemoteException,JvnException
     **/
    public void jvnInvalidateReader(int joi) throws java.rmi.RemoteException, JvnException {
        if (joiToJvnObject.containsKey(joi)){
            joiToJvnObject.get(joi).jvnInvalidateReader();
        } else {
            throw new JvnException("Tried to invalidate inexisting object "+joi);
        }
    }

    /**
     * Invalidate the Write lock of the JVN object identified by id
     *
     * @param joi : the JVN object id
     * @return the current JVN object state
     * @throws java.rmi.RemoteException,JvnException
     **/
    public Serializable jvnInvalidateWriter(int joi) throws java.rmi.RemoteException, JvnException {
        Serializable updatedObject = null;
        if (joiToJvnObject.containsKey(joi)){
            updatedObject = joiToJvnObject.get(joi).jvnInvalidateWriter();
        } else {
            throw new JvnException("Tried to invalidate inexisting object");
        }
        return updatedObject;
    }

    /**
     * Reduce the Write lock of the JVN object identified by id
     *
     * @param joi : the JVN object id
     * @return the current JVN object state
     * @throws java.rmi.RemoteException,JvnException
     **/
    public Serializable jvnInvalidateWriterForReader(int joi) throws java.rmi.RemoteException, JvnException {
        Serializable updatedObject = null;
        if (joiToJvnObject.containsKey(joi)){
            updatedObject = joiToJvnObject.get(joi).jvnInvalidateWriterForReader();
        } else {
            throw new JvnException("Tried to invalidate inexisting object");
        }
        return updatedObject;
    }
}

 
