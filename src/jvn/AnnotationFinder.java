package jvn;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Optional;

/**
 * A function library for finding Annotation in a class and the interfaces it implements
 */
public class AnnotationFinder {
    /**
     * Check if the annotationClass is present on the class objectClass and on the interfaces it implements
     * @param objectClass
     * @param annotationClass
     * @return
     */
    public static  boolean isAnnotationPresentNested(Class<?> objectClass, Class<? extends Annotation> annotationClass){
        return objectClass.isAnnotationPresent(annotationClass) ||
                Arrays.stream(objectClass.getInterfaces()).anyMatch(i -> i.isAnnotationPresent(annotationClass));
    }

    /**
     * Check if the annotationClass is present on the method and on the interfaces its class implements
     * @param method
     * @param annotationClass
     * @return
     */
    public static  boolean isAnnotationPresentNested(Method method, Class<? extends Annotation> annotationClass){
        return method.isAnnotationPresent(annotationClass) ||
                Arrays.stream(method.getDeclaringClass().getInterfaces()).anyMatch(i -> {
                    try {
                        return i.getDeclaredMethod(method.getName(),method.getParameterTypes()).isAnnotationPresent(annotationClass);
                    } catch (NoSuchMethodException e) {
                        e.printStackTrace();
                        return false;
                    }
                });
    }

    /**
     * Get the Annotation of type  annotationClass present on the method or on the interfaces its class implements
     * @param objectClass
     * @param annotationClass
     * @param <T>
     * @return an Annotation of type annotationClass if found else null
     */
    public static  <T extends Annotation> T getAnnotationNested(Class<?> objectClass, Class<T> annotationClass){
        T res = null;
        if (objectClass.isAnnotationPresent(annotationClass)){
            res = objectClass.getAnnotation(annotationClass);
        }

        Optional<Class<?>> opt = Arrays.stream(objectClass.getInterfaces()).filter(i -> i.isAnnotationPresent(annotationClass)).findFirst();

        if (opt.isPresent()) {
            res = opt.get().getAnnotation(annotationClass);
        }

        return res;
    }

    /**
     * Get the Annotation of type annotationClass present on the method or on the interfaces its class implements
     * @param method
     * @param annotationClass
     * @param <T>
     * @return an Annotation of type annotationClass if found else null
     */
    public static <T extends Annotation> T getAnnotationNested(Method method, Class<T> annotationClass){
        T res = null;
        if (method.isAnnotationPresent(annotationClass)){
            res = method.getAnnotation(annotationClass);
        }
        Optional<Class<?>> opt = Arrays.stream(method.getDeclaringClass().getInterfaces()).filter(i -> {
            try {
                return i.getDeclaredMethod(method.getName(),method.getParameterTypes()).isAnnotationPresent(annotationClass);
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
                return false;
            }
        }).findFirst();

        if (opt.isPresent()) {
            res = opt.get().getAnnotation(annotationClass);
        }

        return res;
    }

}
