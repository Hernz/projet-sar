package coordinator;

import jvn.coordinator.JvnCoordImpl;
import jvn.coordinator.JvnPermissions;
import jvn.object.JvnObject;
import jvn.server.JvnRemoteServer;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;
import java.util.Scanner;

public class Coordinator {

    public static void main(String[] argv) {

        // INIT
        try {
            int port = 1099;
            JvnCoordImpl coordinator = JvnCoordImpl.jvnGetCoord(port);

            // Create RMI registry
            Registry registry = LocateRegistry.createRegistry(Registry.REGISTRY_PORT);
            // Bind coordinator to registery
            registry.bind("jvnCoordinator", coordinator);
            System.err.println("Server ready");

            // INTERPRETER
            boolean stop = false;
            String input;
            Scanner scanner = new Scanner(System.in);
            String jon;
            while (!stop) {
                System.out.print("> ");
                input = scanner.nextLine();
                switch (input) {
                    case "info":
                    case "help":
                        System.out.println("Coordinator is on port " + port);
                        System.out.println("RMI registry is on port " + Registry.REGISTRY_PORT);
                        System.out.println("Commands:");
                        System.out.println("info : info about the server");
                        System.out.println("lserv : list connected server");
                        System.out.println("lobj : list managed objects");
                        System.out.println("lorph : list orphan objects");
                        System.out.println("stop : stop the coordinator and exit");
                        break;
                    case "lserv":
                        System.out.println("Known server list:");
                        ArrayList<JvnRemoteServer> servers = coordinator.getKnownServers();
                        for (JvnRemoteServer server : servers) {
                            System.out.println(server);
                        }
                        break;
                    case "lobj":
                        JvnPermissions permissions;
                        for (Integer joi : coordinator.getJoiToPermissions().keySet()) {
                            jon = coordinator.getJoiToJon().get(joi);
                            permissions = coordinator.getJoiToPermissions().get(joi);
                            System.out.println("JON: " + jon);
                            System.out.println("JOI: " + joi);
                            System.out.println(permissions);
                            System.out.println("----------------------------------------------------------------------");
                        }
                        break;
                    case "lorph":
                        JvnObject object;
                        for (Integer joi : coordinator.getOrphans().keySet()) {
                            jon = coordinator.getJoiToJon().get(joi);
                            object = coordinator.getOrphans().get(joi);
                            System.out.println("JON: " + jon);
                            System.out.println("JOI: " + joi);
                            System.out.println(object);
                            System.out.println("----------------------------------------------------------------------");
                        }
                        break;
                    case "stop":
                        stop = true;
                        break;
                }
            }
            scanner.close();

            System.err.print("Server stopped");
            System.exit(0);
        } catch (Exception e) {
            System.err.println("Server exception: " + e.toString());
            e.printStackTrace();
        }
    }
}
