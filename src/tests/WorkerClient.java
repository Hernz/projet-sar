package tests;

import irc.ISentence;
import irc.ISentenceDebug;
import irc.SentenceDebug;
import jvn.JvnException;
import jvn.object.JvnObjectProxy;
import jvn.server.JvnLocalServer;
import jvn.server.JvnServerImpl;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

public class WorkerClient {

    public static void main(String argv[]) {
        if (argv.length < 3 ){
            System.err.println("[ERROR] Worker client expected 3 argument, got "+argv.length);
            System.exit(1);
        }

        String host = "localhost";
        int port = 1099;

        // Create JvnServer
        JvnLocalServer js = JvnServerImpl.jvnGetServer();
        // Connect to coordinator
        js.jvnConnectToCoord(host, port);

        // Get arguments
        int id = Integer.parseInt(argv[0]);
        int nb_objects = Integer.parseInt(argv[1]);
        int nb_access = Integer.parseInt(argv[2]);
        ArrayList<ISentenceDebug> objects = new ArrayList<>();

        final int SLEEP_MIN = 0;
        final int SLEEP_MAX = 100;


        int sleepTime; // Wait time in milliseconds

        // create or fetch nb_objects sentences
        for(int i = 0; i < nb_objects; i++){
            objects.add((ISentenceDebug) JvnObjectProxy.newInstance(new SentenceDebug(),"OBJ-"+i));
            System.out.println("CLIENT-"+ id +" : created OBJ-"+i);
        }

        // Run nb_access access, each on random objects
        int objectId;
        ISentenceDebug sentence;
        LocalDateTime currentTime;
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm:ss.SSS");
        for(int i = 0; i < nb_access; i++){
            objectId = ThreadLocalRandom.current().nextInt(0, objects.size());
            sleepTime = ThreadLocalRandom.current().nextInt(SLEEP_MIN, SLEEP_MAX);
            sentence = objects.get(objectId);
            if(ThreadLocalRandom.current().nextBoolean()){
                // Try to READ
                System.out.println("CLIENT-"+ id +" R OBJ-"+ objectId +" : "+ sentence.read());
                try {
                    TimeUnit.MILLISECONDS.sleep(sleepTime);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                sentence.unlock();
            } else {
                // Try to WRITE
                currentTime = LocalDateTime.now();
                sentence.write("CLIENT-"+id+" : "+ currentTime.format(dtf));
                System.out.println("CLIENT-"+id+" W OBJ-"+ objectId +" : CLIENT-"+id+" : "+ currentTime.format(dtf));
                try {
                    TimeUnit.MILLISECONDS.sleep(sleepTime);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                sentence.unlock();
            }
        }

        try {
            js.jvnTerminate();
        } catch (JvnException e) {
            e.printStackTrace();
        }
        System.out.println("CLIENT-"+id+" FINISHED");
        System.exit(0);
    }
}
