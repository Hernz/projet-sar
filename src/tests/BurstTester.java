package tests;

import java.io.IOException;
import java.util.ArrayList;

public class BurstTester {

    public static void main(String argv[]) {

        final int NB_CLIENTS = 2;
        final int NB_OBJECTS = 2;
        final int NB_ACCESS = 4;

        System.out.println("Burst testing with "+NB_CLIENTS+" clients, "
                +NB_OBJECTS+" shared objects and "+NB_ACCESS+" random access per client.");

        ArrayList<Process> runningProcess = new ArrayList<>();
        ProcessBuilder processBuilder;
        for(int clientId = 0; clientId < NB_CLIENTS; clientId++){
            processBuilder = new ProcessBuilder(
                    "java",
                            "-Dfile.encoding=UTF-8",
                            "-classpath",
                            "bin",
                            "tests.WorkerClient",
                            String.valueOf(clientId+1),
                            String.valueOf(NB_OBJECTS),
                            String.valueOf(NB_ACCESS)
            );
            processBuilder.inheritIO();
            try {
                runningProcess.add(processBuilder.start());
            } catch (IOException e) {
                System.err.println("Could not start CLIENT-"+clientId);
                e.printStackTrace();
            }
        }

        for(Process process : runningProcess){
            try {
                process.waitFor();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        System.exit(0);
    }
}
