package irc;

import jvn.JvnException;
import jvn.server.JvnLocalServer;

import java.awt.*;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;


public class IrcGui extends Frame implements WindowListener {

    public TextArea text;
    public TextField data;
    Frame frame;
    ISentenceDebug sentence;
    JvnLocalServer jvnLocalServer;


    Button read_button;
    Button write_button;
    Button unlock_button;
    Button flush_button;

    /**
     * IRC Constructor
     *
     * @param managedObject the managed object representing the Chat
     **/

    public IrcGui(ISentenceDebug managedObject, JvnLocalServer jvnLocalServer){
        this.jvnLocalServer = jvnLocalServer;
        sentence = managedObject;
        addWindowListener(this);
        setLayout(new GridLayout(1, 1));
        text = new TextArea(10, 60);
        text.setEditable(false);
        text.setForeground(Color.red);
        add(text);
        data = new TextField(40);
        add(data);
        read_button = new Button("read");
        read_button.addActionListener(new IrcController.ReadListener(this));
        add(read_button);
        write_button = new Button("write");
        write_button.addActionListener(new IrcController.WriteListener(this));
        add(write_button);
        unlock_button = new Button("unlock");
        unlock_button.addActionListener(new IrcController.UnlockListener(this));
        add(unlock_button);
        flush_button = new Button("flush");
        flush_button.addActionListener(new IrcController.FlushListener(this));
        add(flush_button);
        setSize(545, 201);
        //setBackground(Color.black);
        setVisible(true);
    }

    @Override
    public void windowOpened(WindowEvent windowEvent) {
        //System.out.println("windowOpened");
    }

    @Override
    public void windowClosing(WindowEvent windowEvent) {
        //System.out.println("windowClosing");
        try {
            jvnLocalServer.jvnTerminate();
        } catch (JvnException e) {
            e.printStackTrace();
        }
        dispose();

    }

    @Override
    public void windowClosed(WindowEvent windowEvent) {
        //System.out.println("windowClosed");
        System.exit(0);
    }

    @Override
    public void windowIconified(WindowEvent windowEvent) {
        //System.out.println("windowIconified");
    }

    @Override
    public void windowDeiconified(WindowEvent windowEvent) {
        //System.out.println("windowDeiconified");
    }

    @Override
    public void windowActivated(WindowEvent windowEvent) {
        //System.out.println("windowActivated");
    }

    @Override
    public void windowDeactivated(WindowEvent windowEvent) {
        //System.out.println("windowDeactivated");
    }
}
