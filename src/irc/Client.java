/***
 * Irc class : simple implementation of a chat using JAVANAISE 
 * Contact: 
 *
 * Authors: 
 */

package irc;


import jvn.object.JvnObjectProxy;
import jvn.server.JvnLocalServer;
import jvn.server.JvnServerImpl;


public class Client {

    /**
     * main method
     * create a JVN object nammed IRC for representing the Chat application
     **/
    public static void main(String argv[]) {
        try {

            String host = "localhost";
            int port = 1099;

            // Create JvnServer
            JvnLocalServer js = JvnServerImpl.jvnGetServer();
            // Connect to coordinator
            js.jvnConnectToCoord(host, port);

            // Create or fetch in the coordinator the sentence object
            ISentenceDebug sentence = (ISentenceDebug) JvnObjectProxy.newInstance(new SentenceDebug(),"IRC");

            // Launch the GUI
            new IrcGui(sentence,js);

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("IRC problem : " + e.getMessage());
        }
    }
}








