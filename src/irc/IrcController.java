package irc;

import jvn.JvnException;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class IrcController {

    /**
     * Internal class to manage user events (read) on the CHAT application
     **/
    static class ReadListener implements ActionListener {
        IrcGui irc;

        public ReadListener(IrcGui i) {
            irc = i;
        }

        /**
         * Management of user events
         **/
        @Override
        public void actionPerformed(ActionEvent e) {
            irc.unlock_button.setEnabled(true);
            irc.write_button.setEnabled(false);
            irc.read_button.setEnabled(true);
            String s = irc.sentence.read();

            // display the read value
            irc.data.setText(s);
            irc.text.append(s + "\n");
        }
    }

    /**
     * Internal class to manage user events (write) on the CHAT application
     **/
    static class WriteListener implements ActionListener {
        IrcGui irc;

        public WriteListener(IrcGui i) {
            irc = i;
        }

        /**
         * Management of user events
         **/
        @Override
        public void actionPerformed(ActionEvent e) {
            irc.unlock_button.setEnabled(true);
            irc.read_button.setEnabled(false);
            irc.write_button.setEnabled(true);
            // get the value to be written from the buffer
            String s = irc.data.getText();

            irc.sentence.write(s);
        }
    }

    /**
     * Internal class to manage user events (unlock) on the CHAT application
     */
    public static class UnlockListener implements ActionListener {
        IrcGui irc;

        public UnlockListener(IrcGui i) {
            irc = i;
        }

        /**
         * Management of user events
         **/
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            irc.unlock_button.setEnabled(false);
            irc.read_button.setEnabled(true);
            irc.write_button.setEnabled(true);
            irc.sentence.unlock();
        }
    }

    /**
     * Internal class to manage user events (flush) on the CHAT application
     */
    public static class FlushListener implements ActionListener {
        IrcGui irc;

        public FlushListener(IrcGui i) { irc = i;}

        /**
         * Management of user events
         **/
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            try {
                irc.jvnLocalServer.jvnFlush();
            } catch (JvnException e) {
                e.printStackTrace();
            }
        }
    }
}
