package irc;

import jvn.annotations.*;

@JvnManaged
public interface ISentence {

    @JvnWrite
    public void write(String text);

    @JvnRead
    public String read();
}
