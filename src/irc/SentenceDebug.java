/***
 * Sentence class : used for keeping the text exchanged between users
 * during a chat application
 * Contact: 
 *
 * Authors: 
 */

package irc;

public class SentenceDebug implements ISentenceDebug, java.io.Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    String data;

    public SentenceDebug() {
        data = new String("Hello, world!");
    }

    public void write(String text) {
        data = text;
    }

    public String read() {
        return data;
    }

    @Override
    public void unlock() {}

}
