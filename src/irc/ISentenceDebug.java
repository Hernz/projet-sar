package irc;

import jvn.annotations.*;

@JvnManaged
@JvnDebug(manualUnlock = true)
public interface ISentenceDebug {

    @JvnWrite
    public void write(String text);

    @JvnRead
    public String read();

    @JvnUnlock
    public void unlock();
}
